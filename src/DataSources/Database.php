<?php

namespace artbyrab\tegoExample\DataSources;

use artbyrab\tego\DataSourceInterface;

/**
 * Database
 * 
 * @author artbyrab
 */
class Database implements DataSourceInterface
{
    /**
     * {@inheritDoc}
     */
    public function getTitle(): string
    {
        return "Database";
    }

    /**
     * {@inheritDoc}
     */
    public function getDescription(): string
    {
        return "The database should be checked for identifiers related to the 
        entity.";
    }
}