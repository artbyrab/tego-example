<?php

namespace artbyrab\tegoExample\Documents;

use artbyrab\tego\DocumentInterface;

/**
 * Data policy
 * 
 * @author artbyrab
 */
class DataPolicy implements DocumentInterface
{
    /**
     * {@inheritDoc}
     */
    public function getTitle(): string
    {
        return "Data policy";
    }

    /**
     * {@inheritDoc}
     */
    public function getDescription(): string
    {
        return "Our data policy for internal staff.";
    }

    /**
     * {@inheritDoc}
     */
    public function getContent()
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function getPath()
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function getUrl()
    {
        return 'http://localhost:8037/data-policy';
    }

    /**
     * {@inheritDoc}
     */
    public function getLocation()
    {
        return false;
    }
}