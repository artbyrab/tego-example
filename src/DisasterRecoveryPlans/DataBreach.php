<?php

namespace artbyrab\tegoExample\DisasterRecoveryPlans;

use artbyrab\tego\DisasterRecoveryPlanInterface;

/**
 * Data breach
 * 
 * @author artbyrab
 */
class DataBreach implements DisasterRecoveryPlanInterface
{
    /**
     * {@inheritDoc}
     */
    public function getTitle(): string
    {
        return "Data breach disaster recovery plan";
    }

    /**
     * {@inheritDoc}
     */
    public function getDescription(): string
    {
        return "This plan will provide steps in order to deal with a 
        data breach.";
    }

    /**
     * {@inheritDoc}
     */
    public function getItems(): array
    {
        return [
            "Set app to maintenance mode",
            "Inform management of potential data breach",
            "Send out the business potential data breach email",
            "Shut down SQL data server",
            "Review SQL data server backup logs",
            "Confirm if data breach did occur",
            "Determine the reach of data breach",
            "Make a plan of action",
            "Review the breach time limit on our Data Regulatory bodies",
            "Inform Data Regulatory bodies where applicable",
            "Review if we need to inform individually affected entities like users or customers",
            "Make neccessary security improvments to any servers before putting them back online",
        ];
    }
}