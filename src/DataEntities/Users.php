<?php

namespace artbyrab\tegoExample\DataEntities;

use artbyrab\tego\DataEntityInterface;
use artbyrab\tegoExample\DataSources\Database;
use artbyrab\tegoExample\DataSources\Email;

/**
 * User
 * 
 * @author artbyrab
 */
class Users implements DataEntityInterface
{
    /**
     * {@inheritDoc}
     */
    public function getTitle(): string
    {
        return "User";
    }

    /**
     * {@inheritDoc}
     */
    public function getDescription(): string
    {
        return "A user is an entity that uses our app and has a login 
        account.";
    }

    /**
     * {@inheritDoc}
     */
    public function getDataSources(): array
    {
        $database = new Database();
        $email = new Email();

        return [
            $database,
            $email
        ];
    }
}