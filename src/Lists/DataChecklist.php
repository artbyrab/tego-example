<?php

namespace artbyrab\tegoExample\Lists;

use artbyrab\tego\ListInterface;

/**
 * Data checklist
 * 
 * @author artbyrab
 */
class DataChecklist implements ListInterface
{
    /**
     * {@inheritDoc}
     */
    public function getTitle(): string
    {
        return "Data Checklist";
    }

    /**
     * {@inheritDoc}
     */
    public function getDescription(): string
    {
        return "Our data checklist provides an overview of things we consider
        related to data.";
    }

    /**
     * {@inheritDoc}
     */
    public function getItems(): array
    {
        return [
            "What data we collect and store",
            "Our methods of data collection",
            "Our reasoning for collecting data",
            "How we keep data secure",
            "What we use the data for",
            "Do we collect any data which requires special attention",
            "Do we transfer any data",
        ];
    }
}