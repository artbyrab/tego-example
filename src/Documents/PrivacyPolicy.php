<?php

namespace artbyrab\tegoExample\Documents;

use artbyrab\tego\DocumentInterface;

/**
 * Privacy policy
 * 
 * @author artbyrab
 */
class PrivacyPolicy implements DocumentInterface
{
    /**
     * {@inheritDoc}
     */
    public function getTitle(): string
    {
        return "Privacy policy";
    }

    /**
     * {@inheritDoc}
     */
    public function getDescription(): string
    {
        return "The public privacy policy for site users.";
    }

    /**
     * {@inheritDoc}
     */
    public function getContent()
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function getPath()
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function getUrl()
    {
        return 'http://localhost:8037/privacy-policy';
    }

    /**
     * {@inheritDoc}
     */
    public function getLocation()
    {
        return false;
    }
}