<?php

namespace artbyrab\tegoExample\Personnel;

use artbyrab\tego\DataPersonnelInterface;

/**
 * Data officer
 * 
 * @author artbyrab
 */
class DataOfficer implements DataPersonnelInterface
{
    /**
     * {@inheritDoc}
     */
    public function getTitle(): string
    {
        return "Data Officer";
    }

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return "Jane Doe";
    }

    /**
     * {@inheritDoc}
     */
    public function getEmail()
    {
        return "dataSupport@tego.tego";
    }

    /**
     * {@inheritDoc}
     */
    public function getPhoneNumber()
    {
        return "+44 7777 777777";
    }
}