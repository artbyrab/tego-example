<?php

namespace artbyrab\tegoExample;

use artbyrab\tego\SummaryInterface;
use artbyrab\tegoExample\ContactInformation;
use artbyrab\tegoExample\DataAudit\DataAudit;
use artbyrab\tegoExample\DataEntities\Customer;
use artbyrab\tegoExample\DataEntities\Users;
use artbyrab\tegoExample\DataRegulations\Gdpr;
use artbyrab\tegoExample\DataSources\Database;
use artbyrab\tegoExample\DataSources\Email;
use artbyrab\tegoExample\DisasterRecoveryPlans\DataBreach;
use artbyrab\tegoExample\Documents\DataPolicy;
use artbyrab\tegoExample\Documents\PrivacyPolicy;
use artbyrab\tegoExample\Documents\TermsAndConditions;
use artbyrab\tegoExample\Lists\DataChecklist;
use artbyrab\tegoExample\Personnel\DataOfficer;


/**
 * Summary
 * 
 * This will provide an overview of all the information covered in the 
 * example.
 * 
 * @author artbyrab
 */
class Summary implements SummaryInterface
{
    /**
     * {@inheritDoc}
     */
    public function getDocuments()
    {
        $documents = array_merge($this->getPublicDocuments(), $this->getInternalDocuments());
      
        return $documents;
    }

    /**
     * {@inheritDoc}
     */
    public function getPublicDocuments()
    {
        $privacyPolicy = new PrivacyPolicy();
        $termsAndConditions = new TermsAndConditions();

        return [
            $privacyPolicy,
            $termsAndConditions
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getInternalDocuments()
    {
        $dataPolicy = new DataPolicy();

        return [
            $dataPolicy,
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getDataRegulations()
    {
        $gdpr = new Gdpr();

        return [
            $gdpr
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getLists()
    {
        $dataChecklist = new DataChecklist();
        
        return [
            $dataChecklist
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getContactInformation()
    {
        $contactInformation = new ContactInformation();

        return $contactInformation;
    }

    /**
     * {@inheritDoc}
     */
    public function getDataEntities()
    {
        $customers = new Customer();
        $users = new Users();

        return [
            $customers,
            $users
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getPersonnel()
    {
        $dataOfficer = new DataOfficer();

        return [
            $dataOfficer
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getDataSources()
    {
        $database = new Database();
        $email = new Email();

        return [
            $database,
            $email
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getDisasterRecoveryPlans()
    {
        $dataBreach = new DataBreach();

        return [
            $dataBreach
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getAudit()
    {
        $dataAudit = new DataAudit;

        return $dataAudit;
    }

    /**
     * {@inheritDoc}
     */
    public function getAuditLogs()
    {
        return false;
    }
}