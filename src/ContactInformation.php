<?php

namespace artbyrab\tegoExample;

use artbyrab\tego\ContactInformationInterface;

/**
 * Contact Information
 * 
 * @author artbyrab
 */
class ContactInformation implements ContactInformationInterface
{
    const EMAIL = "dataSupport@tego.tego";

    /**
     * {@inheritDoc}
     */
    public function getEmail(): string
    {
        return self::EMAIL;
    }

    /**
     * {@inheritDoc}
     */
    public function getDataRequestEmail(): string
    {
        return self::EMAIL;
    }

    /**
     * {@inheritDoc}
     */
    public function getAddress(): string
    {
        return "1 High Street, London, Greater London";
    }
}