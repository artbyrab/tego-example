<?php

namespace artbyrab\tegoExample\Documents;

use artbyrab\tego\DocumentInterface;

/**
 * Terms and conditions
 * 
 * @author artbyrab
 */
class TermsAndConditions implements DocumentInterface
{
    /**
     * {@inheritDoc}
     */
    public function getTitle(): string
    {
        return "Terms and conditions";
    }

    /**
     * {@inheritDoc}
     */
    public function getDescription(): string
    {
        return "The public terms and conditions for users of the site.";
    }

    /**
     * {@inheritDoc}
     */
    public function getContent()
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function getPath()
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function getUrl()
    {
        return 'http://localhost:8037/terms-and-conditions';
    }

    /**
     * {@inheritDoc}
     */
    public function getLocation()
    {
        return false;
    }
}