<?php

namespace artbyrab\tegoExample\DataSources;

use artbyrab\tego\DataSourceInterface;

/**
 * Email
 * 
 * @author artbyrab
 */
class Email implements DataSourceInterface
{
    /**
     * {@inheritDoc}
     */
    public function getTitle(): string
    {
        return "Email";
    }

    /**
     * {@inheritDoc}
     */
    public function getDescription(): string
    {
        return "Emails should be checked incase they reference a data 
        identifier for an entity. This includes any email data stored in the
        database and also internal staff sent folders.";
    }
}