# Tego example

This is an example of using Tego as a base to build a basic data regulation for your app.

## Usage

* Git clone the repo
    * $ git clone artbyrab/tego-example
* Run the local webserver via PHP's built in server

```shell
$ cd ~/public_html/artbyrab/tego-example/public
$ php -S localhost:8000
```

* Run via Docker and docker-compose
    * See the documents/guides/running-the-local-webserver-example.md
* Or review the examples of using Tego in the src folder

## Resources

* Tego
    * artbyrab/tego