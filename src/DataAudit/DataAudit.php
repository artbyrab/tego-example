<?php

namespace artbyrab\tegoExample\DataAudit;

use artbyrab\tego\DataAuditInterface;

/**
 * Data Audit
 * 
 * @author artbyrab
 */
class DataAudit implements DataAuditInterface
{
    /**
     * {@inheritDoc}
     */
    public function getFrequency(): string
    {
        return "Monthly";
    }

    /**
     * {@inheritDoc}
     */
    public function getDate(): string
    {
        return "The 1st of every month";
    }

    /**
     * {@inheritDoc}
     */
    public function getItems(): array
    {
        return [
            'Check all data requests have been processed',
            'Ensure backups are working as expected',
            'Ensure historical data is being anonymised automatically'
        ];
    }
}