<?php

namespace artbyrab\tegoExample\DataRegulations;

use artbyrab\tego\DataRegulationInterface;

/**
 * GDPR
 * 
 * @author artbyrab
 */
class Gdpr implements DataRegulationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return "General Data Protection Regulation (GDPR)";
    }

    /**
     * {@inheritDoc}
     */
    public function getUrl(): string
    {
        return "https://en.wikipedia.org/wiki/General_Data_Protection_Regulation";
    }
}