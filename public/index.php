<?php

require ('../vendor/autoload.php');

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

use artbyrab\tegoExample\Summary;

$summary = new Summary();
$documents = $summary->getDocuments();
$lists = $summary->getLists();
$contactInformation = $summary->getContactInformation();
$dataEntities = $summary->getDataEntities();
$personnel = $summary->getPersonnel();
$dataSources = $summary->getDataSources();
$disasterRecoveryPlans = $summary->getDisasterRecoveryPlans();

/**
 * This will provide an example of using Tego as a base to build your data 
 * regulation policy.
 * 
 * @author artbyrab
 */
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>      <html class="no-js"> <!--<![endif]-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="">
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <h1>Tego Example</h1>
        <p>This is a simple example of using Tego as a base to build your data regulation policies.</p>

        <hr>

        <h2>Data Regulation Policy Summary</h2>
        <p>The below content contains information on various parts of our data policy.</p>

        <hr>

        <h3>Documents</h3>
        <p>Below are the documents:</p>

        <?php foreach ($documents as $document) { ?>
            <h4><?=$document->getTitle();?></h4>
            <p><?=$document->getDescription();?></p>
            <ul>
                <li><a href="<?=$document->getUrl();?>"><?=$document->getUrl();?></a></li>
            </ul>
        <?php } ?>

        <hr>

        <h3>Lists</h3>

        <p>Below are our lists relating to data.</p>

        <?php foreach ($lists as $list) { ?>
            <h4><?=$list->getTitle();?></h4>
            <p><?=$list->getDescription();?></p>
            <ul>

            <?php foreach($list->getItems() as $listItem) { ?>
                <li><?=$listItem;?></li>
            <?php }; ?>

            </ul>
        <?php } ?>

        <hr>

        <h3>Contact Information</h3>

        <p>Below is our contact information relating to our data policy.</p>

        <ul>
            <li>Data email: <?=$contactInformation->getEmail();?></li>
            <li>Data request email: <?=$contactInformation->getDataRequestEmail();?></li>
        </ul>

        <hr>

        <h3>Data Entities</h3>

        <p>Below are the data entities we have to consider when it comes to our data policy.</p>

        <?php foreach ($dataEntities as $dataEntity) { ?>
            <h4><?=$dataEntity->getTitle();?></h4>
            <p><?=$dataEntity->getDescription();?></p>
        <?php } ?>

        <hr>

        <h3>Personnel</h3>

        <p>Below are our internal personnel who work on the management of our data policy.</p>

        <?php foreach ($personnel as $singlePersonnel) { ?>

            <h4><?=$singlePersonnel->getTitle();?></h4>

            <ul>
                <li><?=$singlePersonnel->getName();?></li>

                <?php if (!empty($singlePersonnel->getEmail())) { ?>
                    <li><?=$singlePersonnel->getEmail();?></li>
                <?php }; ?>

                <?php if (!empty($singlePersonnel->getPhoneNumber())) { ?>
                    <li>Phone: <?=$singlePersonnel->getPhoneNumber();?></li>
                <?php }; ?>

            </ul>

        <?php } ?>

        <hr>

        <h3>Data Sources</h3>

        <p>Data sources are places where personal data may be stored. Typically 
        these data sources relate to data entities.</p>

        <?php foreach ($dataSources as $dataSource) { ?>
            <h4><?=$dataSource->getTitle();?></h4>
            <p><?=$dataSource->getDescription();?></p>
        <?php } ?>

        <hr>

        <h3>Disaster Recovery Plans</h3>

        <p>Our disaster recovery plans are for emergency situations.</p>

        <?php foreach ($disasterRecoveryPlans as $plan) { ?>
            <h4><?=$plan->getTitle();?></h4>
            <p><?=$plan->getDescription();?></p>
            <ul>

            <?php foreach($plan->getItems() as $item) { ?>
                <li><?=$item;?></li>
            <?php }; ?>

            </ul>
        <?php } ?>

        <hr>

        <h3>Data Audit</h3>

        <p>Our data audit procedure.</p>

        <ul>
            <li>Frequency: <?=$summary->getAudit()->getFrequency();?></li>
            <li>Date: <?=$summary->getAudit()->getDate();?></li>
            <li>Plan:
                <ul>

                    <?php foreach($summary->getAudit()->getItems() as $item) { ?>
                        <li><?=$item;?></li>
                    <?php }; ?>

                </ul>
            </li>
        </ul>

        <hr>

        <h3>Audit Logs</h3>

        <p>Currently we have not defined our audit logs.</p>
        
        <script src="" async defer></script>
    </body>
</html>