# Running the local webserver example

To run the local webserver example you can use PHP's built in web server:

```shell
$ cd ~/public_html/artbyrab/tego-example/public
$ php -S localhost:8000
```

Or you can use Docker via docker-compose:

* Use Docker Compose to build a local dev environment
* Run docker compose build
    * $ docker-compose build
* Bring the current build up so it is live and visible
    * $ docker-compose up -d
* View the site
    * http://localhost:8037
* To bring down:
    * $ docker-compose down
        * This basically runs docker-composer stop, then docker-compose rm -f

Some additional Docker composer commands:

* To stop:
    * $ docker-compose stop
* To remove(delete all data)
    * $ docker-compose rm -f
* To view status
    * $ docker-compose ps