<?php

namespace artbyrab\tegoExample\DataEntities;

use artbyrab\tego\DataEntityInterface;
use artbyrab\tegoExample\DataSources\Database;
use artbyrab\tegoExample\DataSources\Email;

/**
 * Customer
 * 
 * @author artbyrab
 */
class Customer implements DataEntityInterface
{
    /**
     * {@inheritDoc}
     */
    public function getTitle(): string
    {
        return "Customer";
    }

    /**
     * {@inheritDoc}
     */
    public function getDescription(): string
    {
        return "A customer is an entity that uses our service to order goods
        from our store.";
    }

    /**
     * {@inheritDoc}
     */
    public function getDataSources(): array
    {
        $database = new Database();
        $email = new Email();

        return [
            $database,
            $email
        ];
    }
}